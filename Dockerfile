ARG RUST_VERSION="latest"

FROM rust:${RUST_VERSION}
ARG MDBOOK_VERSION="*"

RUN cargo install mdbook --version "${MDBOOK_VERSION}"
